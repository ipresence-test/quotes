<?php

declare(strict_types=1);

use DI\ContainerBuilder;

use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

use App\Application\Auth\JwtAuth;
use App\Domain\Quote\QuoteCollection;
use Psr\Http\Message\ResponseFactoryInterface;
use Slim\Psr7\Factory\ResponseFactory;

return function (ContainerBuilder $cb) {
    $cb->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },
        JwtAuth::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');

            return new JwtAuth(
                (string) $settings['jwt']['issuer'],
                (int) $settings['jwt']['lifetime'],
                (string) $settings['jwt']['private_key'],
                (string) $settings['jwt']['public_key']
            );
        },
        ResponseFactoryInterface::class => \DI\autowire(ResponseFactory::class),
        QuoteCollection::class => \DI\autowire(QuotesFromDiskRepository::class)
    ]);
};
