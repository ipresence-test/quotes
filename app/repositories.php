<?php

declare(strict_types=1);

use DI\ContainerBuilder;

use App\Domain\Quote\QuoteRepository;
use App\Infrastructure\Persistance\Disk\Quote\QuotesFromDiskRepository;

return function (ContainerBuilder $cb) {
    $cb->addDefinitions([
        QuoteRepository::class => \DI\autowire(QuotesFromDiskRepository::class)
    ]);
};
