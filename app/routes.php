<?php

declare(strict_types=1);

// Middleware
use App\Application\Middleware\CacheMiddleware;
use App\Application\Middleware\JwtMiddleware;

// Application
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

use Slim\Views\PhpRenderer;

use App\Application\Actions\Quote\ViewQuoteAction;

return function (App $app) {
    $app->group(getenv("APP_API_PATH"), function (Group $group) {
        /**
         * Public access
         */
        $group->options('/{routes:.+}', function (Request $request, Response $response) {
            return $response;
        });

        /**
         * Open API schema for Swagger (And others)
         */
        $group->get('/schema', function (Request $request, Response $response) {
            $openapi = \OpenApi\scan(APP_SOURCE);

            header('Content-Type: application/json');

            $oaMarkup = $openapi->toJson();

            $response->getBody()->write($oaMarkup);

            return $response;
        });

        /**
         * Swagger UI
         */
        $group->get('/swagger', function (Request $request, Response $response) {
            $renderer = new PhpRenderer(APP_TEMPLATE);

            return $renderer->render($response, "swagger.html");
        });

        /**
         * GET Shout - Return single quote or quote collection
         */
        $group->get('/shout/{name}', ViewQuoteAction::class)
            ->add(CacheMiddleware::class)
            ->add(JwtMiddleware::class);
    });
};
