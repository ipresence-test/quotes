<?php

declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;
use App\Command\ViewQuoteCommand;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        'settings' => [
            'displayErrorDetails' => getenv('APP_ENV') !== "prod",
            'logger' => [
                'name' => 'Quote',
                'path' => __DIR__ . '/../logs/app.log',
                'level' => Logger::DEBUG,
            ],
            'db' => [],
            'jwt' => [
                'issuer' => getenv('API_AUTH_ISSUER'),
                'lifetime' => getenv('API_AUTH_LIFETIME'),
                'private_key' => file_get_contents(ROOT . getenv('API_AUTH_KEY_PRIVATE')),
                'public_key' => file_get_contents(ROOT . getenv('API_AUTH_KEY_PUBLIC')),
            ],
            'encrypt' => [
                'cost' => getenv('ENCRYPTATION_COST')
            ],
        ],
        'commands' => [
            '__default' => ViewQuoteCommand::class,
            'FindQuote' => ViewQuoteCommand::class
        ]
    ]);
};
