<?php

// Define DS
define('DS', DIRECTORY_SEPARATOR);

// Path settings
define('ROOT', __DIR__ . DS . '..' . DS);

define('APP_SOURCE', ROOT . 'src' . DS);
define('APP_PATH', ROOT . 'app' . DS);
define('APP_VENDOR', ROOT . 'vendor' . DS);
define('LOGS_DIR', ROOT . '../logs' . DS);
define('PUBLIC_DIR', ROOT . 'public' . DS);
define('APP_TEMPLATE', APP_SOURCE . DS . 'Template' . DS);