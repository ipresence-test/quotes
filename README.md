# iPresence Tech Test 

---

## Table of Contents

- [Objetive](#objetive)
- [Example](#example)
- [Constraints](#constrains)
- [Requirements](#requirements)
- [Dependencies](#dependencies)
- [Information](#information)
- [Installation](#installation)
- [Use](#use)
- [Test](#test)
- [Support](#Support)

---

## Objective

We want you to implement a REST API that, given a famous person and a count N, returns N quotes from this famous person _shouted_ .

Shouting a quote consists of transforming it to uppercase and adding an exclamation mark at the end. 

We also want to get a cache layer of these quotes in order to avoid hitting our source (which let's imagine is very expensive) twice for the same person given a T time.

## Example 

Given these quotes from Steve Jobs:
- "The only way to do great work is to love what you do.",
- "Your time is limited, so don’t waste it living someone else’s life!"

The returned response should be:
```
curl -s http://awesomequotesapi.com/shout/steve-jobs?limit=2
[
    "THE ONLY WAY TO DO GREAT WORK IS TO LOVE WHAT YOU DO!",
    "YOUR TIME IS LIMITED, SO DON’T WASTE IT LIVING SOMEONE ELSE’S LIFE!"
]
```

## Constraints 

- Count N provided MUST be equal or less than 10. If not, our API should return an error.
- Any framework is allowed but Laravel

## Requirements

- Composer (Tested with 1.10.8)
- PHP LTS (Tested with PHP 7.4.* and PHP 7.3.*)
- Xdebug (For testing coverage)

## Dependencies

- Slim PHP Last version (Current 4.5)
- Dotenv for settings environment settings (.env)
- JWT for security on REST API
- PhpFastCache for Cache request
- PHP-DI for Dependency Inyection for Container (SlimPHP don't use by default)
- Monolog for Logging
- Slim Bridge to work faster with PHP-DI and for compiling for production
- PHP Prophecy mocking
- PHP Unit Watcher
- Swagger for API Docs (OpenAPI)

## Information

### Swagger

Swagger is generated automatically with annotations. So if any file annotations is updated, Swagger will be updated.

You can find Swagger at

http://localhost:8085/api/v1/swagger

And OpenAPI Schema at

http://localhost:8085/api/v1/schema

### Default Settings

This has been testing on default settings on PORT 8085.

### Security

For security reasons, I've put some security in this.
So every route (app/routes.php) that contains JWT Middleware will require the user use some type of authentication.

By default, the next Bearer Token (https://tools.ietf.org/html/rfc6750) has 1 year of lifespan.

eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJmYzNmNjY4LTgyYTktNDNiNC05ZGU4LWQ2ZDZkNzEyZWQ3YiJ9.eyJpc3MiOiJBUFBfVVJMIiwianRpIjoiMmZjM2Y2NjgtODJhOS00M2I0LTlkZTgtZDZkNmQ3MTJlZDdiIiwiaWF0IjoxNTk3NDQ0Mzc4LCJuYmYiOjE1OTc0NDQzNzgsImV4cCI6MTYyODQ0NDM3OCwidXNlciI6InRlc3RAaXByZXNlbmNlLmNvbSIsInVpZCI6MH0.eDzkxNEipngj6WieKeXx7vworG_xZQOFq5cUZ2PBsoqwB49-aYdUbvFSLf6aU230JZSk14yFybao2zNdZv3cTB4PZ5XL7zKyO8ktVzZLRBnxvX38WzgtIgBJDetH93v3ksWvBSci2_MOjfRQySTsfMMCPg__6iJJQaW_47V_g_4MCvs8ZoGhYWNpvaLh_SyblLt7VCKPlKvUF4DWr0tcN5PFLDFkcAtPymxrasapPS2GRo6bp0eUn7Wr_fed7kHIIlxJYSksSJWAGSTjxb0ykMFumqPl53jXZoMjpzB9Ux4wXZzNhiuK8RPpJhbayOdfDclcssnDCS1aaT_rwm1rAg

You can use in this token in Swagger as next image shows (Img from Github)
![Swagger Auth](https://i.stack.imgur.com/Akue1.png)

You can use in this token in Postman as next image shows (Img from Postman)
![Postman Auth](https://assets.postman.com/postman-docs/authorization-types.jpg)

It validates with private.pem and public.pem, so please don't delete those files :)

### Deploy

I have little time to test Docker container. So feel free to be do it if is need.

## Installation

### Clone

- Clone this repo: `git clone https://gitlab.com/ipresence-test/quotes.git`

### Setup

> Change .env.example to .env to set environment constants

```shell
$ cd quotes
```

Linux

```shell
$ mv .env.example .env
```

Windows

```shell
$ rename .env.example .env
```

After this, we can set our customs environment settings if are required

> Update and install this package first

```shell
$ composer install
```

> Then we can start the testing server

```shell
$ composer run start --timeout=0
```

> After that, we need to go to

http://localhost:8085/api/v1/swagger

---

## Use

In main folder project, with dependencies installed (See [Installation](#installation))

### Option 1: Swagger

Once server has started, go to:

http://localhost:8085/api/v1/swagger

Remember to use Bearer token. (See [Information](#information))

### Option 2: Console CLI

Should return two Steve Jobs quote

```shell
$ composer cli FindQuote "Steve Jobs" 2
```

Should return one Steve Jobs quote

```shell
$ composer cli FindQuote "steve-jobs" 1
```

Should return Socrates quote

```shell
$ composer cli FindQuote "Socrates"
```

### Option 3: cURL (Required) with token

First start server

```shell
$ composer run start --timeout=0
```

Then in another console or tab

```shell
$ curl --location --request GET "http://localhost:8085/api/v1/shout/socrates?limit=2" --header "Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJmYzNmNjY4LTgyYTktNDNiNC05ZGU4LWQ2ZDZkNzEyZWQ3YiJ9.eyJpc3MiOiJBUFBfVVJMIiwianRpIjoiMmZjM2Y2NjgtODJhOS00M2I0LTlkZTgtZDZkNmQ3MTJlZDdiIiwiaWF0IjoxNTk3NDQ0Mzc4LCJuYmYiOjE1OTc0NDQzNzgsImV4cCI6MTYyODQ0NDM3OCwidXNlciI6InRlc3RAaXByZXNlbmNlLmNvbSIsInVpZCI6MH0.eDzkxNEipngj6WieKeXx7vworG_xZQOFq5cUZ2PBsoqwB49-aYdUbvFSLf6aU230JZSk14yFybao2zNdZv3cTB4PZ5XL7zKyO8ktVzZLRBnxvX38WzgtIgBJDetH93v3ksWvBSci2_MOjfRQySTsfMMCPg__6iJJQaW_47V_g_4MCvs8ZoGhYWNpvaLh_SyblLt7VCKPlKvUF4DWr0tcN5PFLDFkcAtPymxrasapPS2GRo6bp0eUn7Wr_fed7kHIIlxJYSksSJWAGSTjxb0ykMFumqPl53jXZoMjpzB9Ux4wXZzNhiuK8RPpJhbayOdfDclcssnDCS1aaT_rwm1rAg"
```

Expected response will be:

{
    "statusCode": 200,
    "data": [
        "AN UNEXAMINED LIFE IS NOT WORTH LIVING!"
    ]
}

## Test

To execute test suit, please first install all dependencies, then:

- To leave the test suit watch

```shell
$ composer test
```

- To run just once

```shell
$ composer test:unit
```

---

## Support

Any concerns about design or patterns please contact meroni.damian@gmail.com

---