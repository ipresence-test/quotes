<?php

declare(strict_types=1);

use App\Application\Handlers\HttpErrorHandler;
use App\Application\Handlers\ShutdownHandler;
use App\Application\ResponseEmitter\ResponseEmitter;

use Slim\Factory\ServerRequestCreatorFactory;

use DI\ContainerBuilder;
use DI\Bridge\Slim\Bridge;

use Dotenv\Dotenv;

use App\Application\Middleware\CliMiddleware;

// Import autoload
require __DIR__ . '/../vendor/autoload.php';

// Import constants
require __DIR__ . '/../app/const.php';

// Memory settings
ini_set('memory_limit', '1024M');

// Time, lang and money settings
setlocale(LC_MONETARY, 'en_US');
date_default_timezone_set('UTC');

Dotenv::createImmutable(ROOT)->load();

// Instantiate PHP-DI ContainerBuilder
$containerBuilder = new ContainerBuilder();

if (getenv('APP_ENV') === "prod") {
	// Hide errors on prod
	ini_set('display_errors', '0');
	ini_set('display_startup_errors', '0');
	error_reporting(0);

	// Compile Container for better performance in production env
	$containerBuilder->enableCompilation(__DIR__ . '/../var/cache');
} else {
	// Error notice
	ini_set('display_errors', '1');
	ini_set('display_startup_errors', '1');
	error_reporting(E_ALL);
}

// Set up settings that we will be injected on container
$settings = require __DIR__ . '/../app/settings.php';
$settings($containerBuilder);

// Set up dependencies on container
$dependencies = require __DIR__ . '/../app/dependencies.php';
$dependencies($containerBuilder);

// Set up repositories on container
$repositories = require __DIR__ . '/../app/repositories.php';
$repositories($containerBuilder);

// To improve performance, we skip annotations from Doctrine
$containerBuilder->useAnnotations(false);

// Build PHP-DI Container instance
$container = $containerBuilder->build();

// Instantiate the app
$app = Bridge::create($container);

$callableResolver = $app->getCallableResolver();

// Register middleware
$middleware = require __DIR__ . '/../app/middleware.php';
$middleware($app);

// Register routes
$routes = require __DIR__ . '/../app/routes.php';
$routes($app);

/** @var bool $displayErrorDetails */
$displayErrorDetails = $container->get('settings')['displayErrorDetails'];

// Create Request object from globals
$serverRequestCreator = ServerRequestCreatorFactory::create();
$request = $serverRequestCreator->createServerRequestFromGlobals();

// Create Error Handler
$responseFactory = $app->getResponseFactory();
$errorHandler = new HttpErrorHandler($callableResolver, $responseFactory);

// Create Shutdown Handler
$shutdownHandler = new ShutdownHandler($request, $errorHandler, $displayErrorDetails);
register_shutdown_function($shutdownHandler);

// Add Routing Middleware
$app->addRoutingMiddleware();

// Add Error Middleware
$errorMiddleware = $app->addErrorMiddleware($displayErrorDetails, false, false);
$errorMiddleware->setDefaultErrorHandler($errorHandler);

if (PHP_SAPI === 'cli') {
	$app->add(CliMiddleware::class);
}

// Run App & Emit Response
$response = $app->handle($request);
$responseEmitter = new ResponseEmitter();
$responseEmitter->emit($response);
