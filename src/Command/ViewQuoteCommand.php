<?php

declare(strict_types=1);

namespace App\Command;

use App\Domain\Exception\CliException;
use Psr\Http\Message\ResponseInterface as Response;

use App\Domain\Quote\QuoteRepository;
use App\Common\UrlParameterFormat;
use App\Domain\Exception\QuoteNotFoundException;

class ViewQuoteCommand extends CliCommand
{
    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var Url Formatter
     */
    protected $formatUrl;

    /**
     * @param LoggerInterface $logger
     * @param QuoteRepository  $quoteRepository
     */
    public function __construct(QuoteRepository $quoteRepository, UrlParameterFormat $formatUrl)
    {
        $this->quoteRepository = $quoteRepository;

        $this->formatUrl = $formatUrl;
    }

    protected function action(): Response
    {
        if (!isset($this->args[0]) || !is_string($this->args[0])) {
            throw new CliException("Author name required and string type.\nExample: FindQuote \"benjamin franklin\" 10");
        }

        $name = $this->args[0];

        if (isset($this->args[1])) {
            if (is_numeric($this->args[1])) {
                $limit = (int) $this->args[1];
            } else {
                throw new CliException("Limit must be numeric and less than 10.\nExample: FindQuote \"benjamin franklin\" 10");
            }
        }

        $nameClean = $this->formatUrl::CleanParameter($name);

        try {
            if (isset($limit)) {
                $result = $this->quoteRepository->paginateByAuthor($nameClean, $limit);

                foreach ($result->getQuoteListArrayShout() as $shout) {
                    echo $shout . PHP_EOL;
                }
            } else {
                $result = $this->quoteRepository->findOneByAuthor($nameClean);

                echo $result->shout();
            }
        } catch (QuoteNotFoundException $e) {
            echo $e->getMessage();
        }

        return $this->respond();
    }
}
