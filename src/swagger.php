<?php

/**
 * @OA\Info(
 *     description="API Definitions",
 *     version="1.1.0",
 *     title="API Schema",
 *     @OA\Contact(
 *         email="meroni.damian@gmail.com"
 *     )
 * )
 * 
 * @OA\Server(
 *     description="Local Environment",
 *     url="http://localhost:8085/api/v1"
 * )
 * 
 * @OA\Server(
 *     description="Docker Env",
 *     url="http://localhost:85/api/v1"
 * )
 * 
 * @OA\SecurityScheme(
 *      securityScheme="bearerAuth",
 *      in="header",
 *      name="Authorization",
 *      type="http",
 *      scheme="bearer",
 *      bearerFormat="JWT"
 * )
 */
