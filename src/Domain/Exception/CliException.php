<?php

declare(strict_types=1);

namespace App\Domain\Exception;

use Exception;

class CliException extends Exception
{
    public function __construct(string $message)
    {
        echo $message . PHP_EOL;
        exit;
    }
}
