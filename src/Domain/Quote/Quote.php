<?php

declare(strict_types=1);

namespace App\Domain\Quote;

use App\Domain\Exception\InvalidArgumentException;

class Quote implements QuoteInterface
{
    /**
     * @var string
     */
    protected $quote;

    /**
     * @var string
     */
    protected $author;

    /**
     * Max items on list
     *
     * @var integer
     */
    const LIMIT_PER_REQUEST = 10;

    /**
     * @param string    $quote
     * @param string    $author
     */
    public function __construct(string $author, string $quote)
    {
        if (empty($author) || empty($quote)) {
            throw new InvalidArgumentException('Missing parameters');
        }

        $this->author = $author;
        $this->quote = $quote;
    }

    /**
     * @return string
     */
    public function getQuote(): string
    {
        return $this->quote ?? null;
    }

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author ?? null;
    }

    /**
     * @return string
     */
    public function shout(): ?string
    {
        if ($this->quote) {
            return $this->_formatShout();
        }

        return null;
    }

    /**
     * Give shout custom format
     *
     * Please, if you going to extend (refactor) this, keep in mind SOLID
     * Apply State Strategy, Polymorphism or subclasses.
     * 
     * Damian.
     * 
     * @return void
     */
    private function _formatShout()
    {
        if (!is_string($this->quote)) {
            return false;
        }

        $str = trim($this->quote);

        if (!empty($str)) {
            // We validate if ends with ellipsis
            if (strlen($str) > 2 && substr($str, -3) === "...") {
                $formatQuote = mb_strtoupper($str) . '!';
            } else {
                $lastChar = $str[strlen($str) - 1];

                switch ($lastChar) {
                    case '.':
                        $str[strlen($str) - 1] = '!';
                        $formatQuote = mb_strtoupper($str);
                        break;
                    case $lastChar === "!":
                        $formatQuote = mb_strtoupper($str);
                        break;
                    default:
                        $formatQuote = mb_strtoupper($str) . '!';
                }
            }
        }

        return $formatQuote;
    }
}
