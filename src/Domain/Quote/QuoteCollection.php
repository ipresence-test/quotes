<?php

declare(strict_types=1);

namespace App\Domain\Quote;

use App\Domain\Exception\InvalidArgumentException;

class QuoteCollection implements QuoteCollectionInterface
{
    protected $quotes = [];
    protected $key = 0;

    public function __construct(QuoteInterface ...$quotes)
    {
        foreach ($quotes as $quote) {
            $this->offsetSet(null, $quote);
        }
    }

    /**
     * Add new quote to collection
     *
     * @param QuoteInterface $quote
     * @return void
     */
    public function add(QuoteInterface $quote)
    {
        $this->offsetSet(null, $quote);
    }

    /**
     * Clear collection
     *
     * @return void
     */
    public function clear()
    {
        $this->quotes = array();
    }

    /**
     * Get messages of each quote in collection and return as array
     */
    public function getQuoteListArrayShout()
    {
        $quoteArray = [];

        if (!empty($this->quotes)) {
            foreach ($this->quotes as $quote) {
                $quoteArray[] = $quote->shout();
            }
        }

        return $quoteArray;
    }

    /**
     * From PHP \Countable Interface
     */
    public function count()
    {
        return count($this->quotes);
    }

    /**
     * From PHP \ArrayAccess Interface
     */
    public function offsetSet($key, $value)
    {
        if (!$value instanceof QuoteInterface) {
            throw new InvalidArgumentException(
                "Could not add the quote to the collection."
            );
        }

        if (!isset($key)) {
            $this->quotes[] = $value;
        } else {
            $this->quotes[$key] = $value;
        }
    }

    /**
     * From PHP \ArrayAccess Interface
     */
    public function offsetUnset($key)
    {
        if ($key instanceof QuoteInterface) {
            $this->quotes = array_filter(
                $this->quotes,
                function ($v) use ($key) {
                    return $v !== $key;
                }
            );
        } else if (isset($this->quotes[$key])) {
            unset($this->quotes[$key]);
        }
    }

    /**
     * From PHP \ArrayAccess Interface
     */
    public function offsetGet($key)
    {
        if (isset($this->quotes[$key])) {
            return $this->quotes[$key];
        }
    }

    /**
     * From PHP \ArrayAccess Interface
     */
    public function offsetExists($key)
    {
        return ($key instanceof QuoteInterface)
            ? array_search($key, $this->quotes)
            : isset($this->quotes[$key]);
    }

    /**
     * From PHP \IteratorAggregate   Interface
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->quotes);
    }
}
