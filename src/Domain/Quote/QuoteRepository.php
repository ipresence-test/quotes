<?php

declare(strict_types=1);

namespace App\Domain\Quote;

interface QuoteRepository
{
    /**
     * Find one quote by author name
     *
     * @param string $name Author name
     * @return Quote|null
     */
    public function findOneByAuthor(string $name): ?Quote;

    /**
     * Find quotes by author name
     *
     * @param string $name Author name
     * @param integer $qtyLimit Limit for items
     * @return array|null
     */
    public function paginateByAuthor(string $name, int $qtyLimit = 1): ?QuoteCollectionInterface;
}
