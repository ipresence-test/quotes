<?php

namespace App\Domain\Quote;

interface QuoteCollectionInterface extends \Countable, \ArrayAccess, \IteratorAggregate
{
    public function add(QuoteInterface $quote);
    public function clear();
    public function getQuoteListArrayShout();
}
