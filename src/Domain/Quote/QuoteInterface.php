<?php

declare(strict_types=1);

namespace App\Domain\Quote;

interface QuoteInterface
{
    /**
     * @return string
     */
    public function getQuote(): string;

    /**
     * @return string
     */
    public function getAuthor(): string;

    /**
     * @return string
     */
    public function shout(): ?string;
}
