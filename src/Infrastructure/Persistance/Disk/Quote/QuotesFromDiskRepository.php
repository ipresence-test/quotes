<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistance\Disk\Quote;

use App\Domain\Quote\Quote;
use App\Domain\Exception\QuoteNotFoundException;
use App\Domain\Quote\QuoteRepository;
use App\Common\UrlParameterFormat;
use App\Domain\Exception\LimitPerRequestException;
use App\Common\FileHandler;

use App\Domain\Quote\QuoteCollection;
use App\Domain\Quote\QuoteCollectionInterface;

class QuotesFromDiskRepository implements QuoteRepository
{
    protected $quoteList;

    public function __construct($list = null)
    {
        $fileHandler = new FileHandler(ROOT . getenv('QUOTE_LIST'));

        $this->quoteList = $list ? $list : $fileHandler->decodeJsonFIle();
    }

    /**
     * Find quotes by author name
     *
     * @param string $name
     * @return Quote
     */
    public function paginateByAuthor(string $name, int $qtyLimit = 1): ?QuoteCollectionInterface
    {
        try {
            if ($this->quoteList) {
                $limit = (isset($qtyLimit)) ? $qtyLimit : 1;

                if ($limit > Quote::LIMIT_PER_REQUEST) {
                    throw new LimitPerRequestException("Limit per request is " . Quote::LIMIT_PER_REQUEST);
                }

                $name = strtolower($name);

                $counter = 0;

                $quoteCollection = new QuoteCollection();

                foreach ($this->quoteList as $quote) {
                    if ($counter < $limit && $name == UrlParameterFormat::CleanParameter($quote->author)) {
                        $counter++;

                        $quoteCollection->add(new Quote($quote->author, $quote->quote));
                    }
                }

                return $counter > 0 ? $quoteCollection : null;
            }

            return null;
        } catch (QuoteNotFoundException $e) {
            throw new QuoteNotFoundException("Quote not found");
        }
    }

    /**
     * Find quote by author name
     *
     * @param string $name
     * @return Quote
     */
    public function findOneByAuthor(string $name): ?Quote
    {
        try {
            if ($this->quoteList) {
                $name = strtolower($name);

                foreach ($this->quoteList as $obj) {
                    if ($name == UrlParameterFormat::CleanParameter($obj->author)) {
                        return empty($obj->quote) ? null : new Quote($obj->author, $obj->quote);
                    }
                }
            }

            throw new QuoteNotFoundException("Quote not found");
        } catch (QuoteNotFoundException $e) {
            throw new QuoteNotFoundException("Quote not found");
        }
    }
}
