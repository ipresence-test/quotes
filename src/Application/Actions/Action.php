<?php

declare(strict_types=1);

namespace App\Application\Actions;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use Psr\Log\LoggerInterface;

use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;

abstract class Action
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var array
     */
    protected $args;

    /**
     * @var array
     */
    protected $params;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Request  $request
     * @param Response $response
     * @param array    $args
     * 
     * @return Response
     * @throws HttpNotFoundException
     * @throws HttpBadRequestException
     */
    public function __invoke(Request $request, Response $response): Response
    {
        $this->request = $request;
        $this->response = $response;

        $this->args = $this->getArgsFromRequest($request);
        $this->params = $this->getQueryParams($request);

        try {
            return $this->action();
        } catch (HttpBadRequestException $e) {
            throw new HttpNotFoundException($this->request, $e->getMessage());
        }
    }

    /**
     * Workaround to get params from request with injection of Slim bridge
     *
     * @param Request $request
     * @return array
     */
    private function getArgsFromRequest(Request $request): array
    {
        $attributes = $request->getAttributes();

        if (isset($attributes["__route__"])) {
            return $attributes["__route__"]->getArguments();
        } else if (isset($attributes["route"])) {
            return $attributes["route"]->getArguments();
        }

        return [];
    }

    /**
     * Get query params from URI
     * 
     * @param Request $request
     * @return array
     */
    private function getQueryParams(Request $request): array
    {
        $query = $request->getQueryParams();

        return !empty($query) ? $query : [];
    }

    /**
     * @return Response
     */
    abstract protected function action(): Response;

    /**
     * @return array|object
     * @throws HttpBadRequestException
     */
    protected function getFormData($toArray = false)
    {
        $input = json_decode(file_get_contents('php://input'), $toArray);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new HttpBadRequestException($this->request, 'Malformed JSON input.');
        }

        return $input;
    }

    /**
     * @param  string $name
     * @return mixed
     * @throws HttpBadRequestException
     */
    protected function resolveArg(string $name)
    {
        if (!isset($this->args[$name])) {
            throw new HttpBadRequestException($this->request, "Could not resolve argument `{$name}`.");
        }

        return $this->args[$name];
    }

    /**
     * @param  array|object|null $data
     * @return Response
     */
    protected function respondWithData($data = null): Response
    {
        $payload = new ActionPayload(200, $data);

        return $this->respond($payload);
    }

    /**
     * @param ActionPayload $payload
     * @return Response
     */
    protected function respond(ActionPayload $payload): Response
    {
        $json = json_encode($payload, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);

        $this->response->getBody()->write($json);

        return $this->response->withHeader('Content-Type', 'application/json; charset=utf-8');
    }

    /**
     * Responde with custom HTTP Code and data
     *
     * @param integer $httpCode
     * @param any $data
     * @return Response
     */
    protected function respondWithCode(int $httpCode, $data = null): Response
    {
        $payload = new ActionPayload($httpCode, $data);

        return $this->respond($payload);
    }

    /**
     * Unauthorized request response
     *
     * @return Response
     */
    protected function respondUnauthorized(): Response
    {
        return $this->response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus(401, 'Unauthorized');
    }
}
