<?php

declare(strict_types=1);

namespace App\Application\Actions\Quote;

use App\Application\Actions\Action;

use App\Domain\Quote\QuoteRepository;

use Psr\Log\LoggerInterface;

use App\Common\UrlParameterFormat;

abstract class QuoteAction extends Action
{
    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var Url Formatter
     */
    protected $formatUrl;

    /**
     * @param LoggerInterface $logger
     * @param QuoteRepository  $quoteRepository
     */
    public function __construct(LoggerInterface $logger, QuoteRepository $quoteRepository, UrlParameterFormat $formatUrl)
    {
        parent::__construct($logger);

        $this->logger = $logger;

        $this->quoteRepository = $quoteRepository;

        $this->formatUrl = $formatUrl;
    }
}
