<?php

declare(strict_types=1);

namespace App\Application\Actions\Quote;

use Psr\Http\Message\ResponseInterface as Response;

/**
 * @OA\Get(
 *      path="/shout/{name}",
 *      tags={"Quote"},
 *      summary="Get Quote by author name",
 *      security={{"bearerAuth":{}}},
 *      @OA\Parameter(
 *          name="name",
 *          in="path",
 *          description="The Quote that need to be fetched",
 *          required=true,
 *          @OA\Schema(
 *              type="string"
 *          )
 *      ),
 *      @OA\Parameter(
 *          name="limit",
 *          in="query",
 *          description="Quotes per request",
 *          @OA\Schema(
 *              type="integer"
 *          )
 *      ),
 *      @OA\Response(response=200, description="successful operation"),
 *      @OA\Response(response=400, description="Invalid quote supplied"),
 *      @OA\Response(response=401, description="Unauthorized"),
 *      @OA\Response(response=404, description="Quote not found")
 * )
 */
class ViewQuoteAction extends QuoteAction
{
    /**
     * Quote Action
     *
     * @return Response
     */
    protected function action(): Response
    {
        $name = $this->resolveArg('name');

        if (empty($name)) {
            return $this->respondWithCode(400, "Author name is required");
        }

        $limit = (isset($this->params['limit']) && is_numeric($this->params['limit'])) ? (int) $this->params['limit'] : null;

        $nameClean = $this->formatUrl::CleanParameter($name);

        if ($limit) {
            return $this->_withLimit($nameClean, $limit);
        } else {
            return $this->_onlyOne($nameClean);
        }
    }

    /**
     * Return quote list in array format by given author name and limit
     *
     * @param string $name
     * @param integer $limit
     * @return void
     */
    private function _withLimit(string $name, int $limit)
    {
        try {
            $result = $this->quoteRepository->paginateByAuthor($name, $limit);

            return $result ? $this->respondWithData($result->getQuoteListArrayShout()) : $this->respondWithCode(404, "Not found");
        } catch (\Exception $e) {
            return $this->respondWithCode(400, $e->getMessage());
        }
    }

    /**
     * Return only one quote by author name
     *
     * @param string $name
     * @return void
     */
    private function _onlyOne(string $name)
    {
        try {
            $result = $this->quoteRepository->findOneByAuthor($name);

            return $result ? $this->respondWithData($result->shout()) : $this->respondWithCode(404, "Not found");
        } catch (\Exception $e) {
            return $this->respondWithCode(400, $e->getMessage());
        }
    }
}
