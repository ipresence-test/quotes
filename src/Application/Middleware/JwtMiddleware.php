<?php

declare(strict_types=1);

namespace App\Application\Middleware;

use App\Application\Auth\JwtAuth;

use Psr\Http\Message\ResponseFactoryInterface;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use Psr\Http\Server\RequestHandlerInterface;

use Psr\Log\LoggerInterface;

use Exception;

final class JwtMiddleware
{
    private $JwtAuth;

    private $ResponseFactory;

    private $Logger;

    public function __construct(
        JwtAuth $JwtAuth,
        ResponseFactoryInterface $responseFactory,
        LoggerInterface $logger
    ) {
        $this->JwtAuth = $JwtAuth;
        $this->ResponseFactory = $responseFactory;
        $this->Logger = $logger;
    }

    /**
     * Invoke middleware.
     *
     * @param ServerRequestInterface $request The request
     * @param RequestHandlerInterface $handler The handler
     *
     * @return ResponseInterface The response
     */
    public function __invoke(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $authorization = explode(' ', (string) $request->getHeaderLine('Authorization'));
        $token = $authorization[1] ?? '';

        try {
            if (!$token || !$this->JwtAuth->validateToken($token)) {
                $this->Logger->info('Unauthorized token ' . $token);

                return $this->ResponseFactory->createResponse()
                    ->withHeader('Content-Type', 'application/json')
                    ->withStatus(401, 'Unauthorized');
            }
        } catch (Exception $e) {
            $this->Logger->info('Bad token format ' . $token);

            return $this->ResponseFactory->createResponse()
                ->withHeader('Content-Type', 'application/json')
                ->withStatus(400, 'Bad token format');
        }

        $parsedToken = $this->JwtAuth->createParsedToken($token);
        $request = $request->withAttribute('token', $parsedToken);

        return $handler->handle($request);
    }
}
