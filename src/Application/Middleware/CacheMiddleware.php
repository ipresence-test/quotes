<?php

declare(strict_types=1);

namespace App\Application\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;

use Phpfastcache\CacheManager;

use Slim\Psr7\Response;

final class CacheMiddleware
{
    /**
     * Invoke middleware.
     *
     * @param ServerRequestInterface $request The request
     * @param RequestHandlerInterface $handler The handler
     *
     * @return ResponseInterface The response
     */
    public function __invoke(Request $request, RequestHandler $handler): ResponseInterface
    {
        $InstanceCache = CacheManager::getInstance('files');

        $key = isset($request->getServerParams()['REQUEST_URI']) ? md5($request->getServerParams()['REQUEST_URI']) : null;

        if ($key) {
            $cacheString = $InstanceCache->getItem($key);

            if (!$cacheString->isHit()) {
                $response = $handler->handle($request);

                $cacheString->set((string) $response->getBody())->expiresAfter((int) getenv('CACHE_EXPIRATION') ?? 0);

                $InstanceCache->save($cacheString);
            } else {
                $response = new Response();

                header("Content-type: application/json");

                $response->getBody()->write($cacheString->get());

                return $response;
            }
        } else {
            $response = $handler->handle($request);
        }

        return $response;
    }
}
