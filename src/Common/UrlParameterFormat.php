<?php

declare(strict_types=1);

namespace App\Common;

class UrlParameterFormat
{
    /**
     * Parse incoming string (Parameter)  
     *
     * @param string $parameter
     * @return void
     */
    public static function CleanParameter(string $parameter): ?string
    {
        if (gettype($parameter) === "string") {
            // Regex leave only letters
            $strParsed = preg_replace("/[^A-Za-z]/", " ", $parameter);

            // Regex Converting extra spaces in one space
            $strParsed = trim(preg_replace('/\s+/', ' ', $strParsed));

            $strParsed = strtolower($strParsed);

            return $strParsed ? $strParsed : null;
        }

        return null;
    }
}
