<?php

declare(strict_types=1);

namespace App\Common;

use App\Domain\Exception\NotFoundException;

class FileHandler
{
    private $fileRoute;

    /**
     * Create a file instance
     *
     * @param string $route
     */
    public function __construct(string $route = null)
    {
        $this->_setFileRoute($route);
    }

    /**
     * Set route
     *
     * @param [type] $route
     * @return void
     */
    private function _setFileRoute($route)
    {
        if (gettype($route) === "string") {
            $this->fileRoute = $route;
        }
    }

    /**
     * Validates if file is writeable
     *
     * @return boolean
     */
    private function _fileIsWriteable(): bool
    {
        try {
            return ($this->fileRoute && is_readable($this->fileRoute));
        } catch (NotFoundException $e) {
            throw new NotFoundException("Error reading file");
        }
    }

    /**
     * Decode json file.
     * 
     * If file not exists, return false
     *
     * @return void
     */
    public function decodeJsonFile()
    {
        try {
            if ($this->_fileIsWriteable()) {
                $contentFile = file_get_contents($this->fileRoute);

                $decodedList = json_decode($contentFile);

                if ((json_last_error() === JSON_ERROR_NONE)) {
                    return property_exists($decodedList, "quotes") ? $decodedList->quotes : $decodedList;
                } else {
                    return false;
                }
            }

            return false;
        } catch (\Exception $e) {
            throw new \Exception('Caught exception: ' . $e->getMessage());
        }
    }
}
