<?php

declare(strict_types=1);

use Dotenv\Dotenv;

// Import autoload
require __DIR__ . '/../vendor/autoload.php';

// Import constants
require __DIR__ . '/../app/const.php';

// Memory settings
ini_set('memory_limit', '1024M');

// Time, lang and money settings
setlocale(LC_MONETARY, 'en_US');
date_default_timezone_set('UTC');

Dotenv::createImmutable(ROOT)->load();
