<?php

declare(strict_types=1);

namespace Tests\Application\Actions;

use Tests\TestCase;

use App\Domain\Quote\Quote;
use App\Domain\Quote\QuoteCollection;
use App\Infrastructure\Persistance\Disk\Quote\QuotesFromDiskRepository;

use Tests\AppTestTrait;

use Prophecy\PhpUnit\ProphecyTrait;
use Phpfastcache\CacheManager;

class ViewQuoteActionTest extends TestCase
{
    use ProphecyTrait;
    use AppTestTrait;

    public function testViewOneWithoutToken()
    {
        $request = $this->createRequest('GET', '/api/v1/shout/Beverly-Sills');

        $response = $this->app->handle($request);

        $this->assertSame(401, $response->getStatusCode());

        CacheManager::clearInstances();
    }

    public function testViewOneWithInvalidToken()
    {
        $request = $this->createRequest('GET', '/api/v1/shout/Beverly-Sills', '', ['Authorization' => 'bearer bad']);

        $response = $this->app->handle($request);

        $this->assertSame(400, $response->getStatusCode());

        CacheManager::clearInstances();
    }

    public function testViewOneQuoteAction()
    {
        $quote = new Quote('Author', 'Quote');

        $this->mock(QuotesFromDiskRepository::class)->method('findOneByAuthor')->willReturn($quote);

        $request = $this->createRequest('GET', '/api/v1/shout/Beverly-Sills', '', $this->token);

        $response = $this->app->handle($request);

        $expected = [
            "statusCode" => 200,
            "data" => "YOU MAY BE DISAPPOINTED IF YOU FAIL, BUT YOU ARE DOOMED IF YOU DON’T TRY!"
        ];

        $this->assertSame(200, $response->getStatusCode());

        $this->assertJsonData($response, $expected);

        CacheManager::clearInstances();
    }

    public function testViewMultipleQuoteAction()
    {
        $quoteCollection = new QuoteCollection(new Quote('Author', 'Quote'), new Quote('Author', 'Quote'));

        $this->mock(QuotesFromDiskRepository::class)->method('paginateByAuthor')->willReturn($quoteCollection);

        $request = $this->createRequest('GET', '/api/v1/shout/steve-jobs', 'limit=3', $this->token);

        $response = $this->app->handle($request);

        $expected = [
            "statusCode" => 200,
            "data" => [
                "YOUR TIME IS LIMITED, SO DON’T WASTE IT LIVING SOMEONE ELSE’S LIFE!",
                "THE ONLY WAY TO DO GREAT WORK IS TO LOVE WHAT YOU DO!"
            ]
        ];

        $this->assertSame(200, $response->getStatusCode());

        $this->assertJsonData($response, $expected);

        CacheManager::clearInstances();
    }
}
