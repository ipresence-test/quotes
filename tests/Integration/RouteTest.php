<?php

declare(strict_types=1);

namespace Tests\Application\Actions;

use Tests\TestCase;

use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;

class RouteTest extends TestCase
{
    public function testServerIsReachable()
    {
        $this->assertEquals(1, 1);
    }
}
