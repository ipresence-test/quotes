<?php

declare(strict_types=1);

namespace Tests\Application\Actions;

use App\Command\ViewQuoteCommand;
use Tests\TestCase;

use Slim\Psr7\Response;

use App\Domain\Quote\Quote;
use App\Domain\Quote\QuoteCollection;
use App\Infrastructure\Persistance\Disk\Quote\QuotesFromDiskRepository;

use Tests\AppTestTrait;

use Prophecy\PhpUnit\ProphecyTrait;

use ReflectionClass;

class ViewQuoteCommandTest extends TestCase
{
    use ProphecyTrait;
    use AppTestTrait;

    public function testQuoteHasActionMethod()
    {
        $task = new ReflectionClass(ViewQuoteCommand::class);

        $this->assertEquals(true, $task->hasMethod('action'));
    }

    public function testQuoteCommand()
    {
        $task_instance = $this->container->get(ViewQuoteCommand::class);

        $response = new Response();

        $request = $this->createRequest('GET', '', '');

        // Args passed like "console positions argsv"
        $args = [
            0 =>  "BENJAMIN FRANKLIN",
            1 => 1
        ];

        $this->expectOutputString("I DIDN’T FAIL THE TEST. I JUST FOUND 100 WAYS TO DO IT WRONG!\r\n");

        $response = $task_instance($request, $response, $args);
    }
}
