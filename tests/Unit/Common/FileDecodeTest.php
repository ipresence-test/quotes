<?php

declare(strict_types=1);

namespace Tests\Unit\Common;

use Tests\TestCase;
use App\Common\FileHandler;

class FileDecodeTest extends TestCase
{
    public function testDecodeFileWithoutFile()
    {
        $fileHandler = new FileHandler();

        $this->assertEquals(false, $fileHandler->decodeJsonFile());
    }

    public function testDecodeFileWithWrongFilename()
    {
        $fileHandler = new FileHandler('test');

        $this->assertEquals(false, $fileHandler->decodeJsonFile());
    }

    public function testDecodeFileFromJsonIsArray()
    {
        $fileHandler = new FileHandler(ROOT . getenv('QUOTE_LIST'));

        $decodedFile = $fileHandler->decodeJsonFile();

        $this->assertEquals(true, is_array($decodedFile));
    }

    public function testDecodeFileHasValues()
    {
        $fileHandler = new FileHandler(ROOT . getenv('QUOTE_LIST'));

        $decodedFile = $fileHandler->decodeJsonFile();

        $this->assertEquals(true, count($decodedFile));
    }
}
