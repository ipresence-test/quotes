<?php

declare(strict_types=1);

namespace Tests\Application\Actions;

use App\Application\Actions\Action;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

use Slim\Exception\HttpNotFoundException;

use Tests\TestCase;

class ActionTest extends TestCase
{
    protected $app;
    protected $container;
    protected $logger;

    protected function setUp(): void
    {
        $this->app = $this->getAppInstance();

        $this->container = $this->app->getContainer();

        $this->logger = $this->container->get(LoggerInterface::class);
    }

    private function _createGetRequest($mock, $params = null)
    {
        $this->app->get('/test-with-code' . ($params ? '/{testParam}' : ''), $mock);

        $request = $this->createRequest('GET', '/test-with-code' . ($params ?? ''));

        return $this->app->handle($request);
    }

    public function testActionSetsHttpCodeRespondCode()
    {
        $response = $this->_createGetRequest(new class ($this->logger) extends Action
        {
            public function __construct(
                LoggerInterface $loggerInterface
            ) {
                parent::__construct($loggerInterface);
            }

            public function action(): Response
            {
                return $this->respondWithCode(200, "Ok");
            }
        });

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testActionSetsHttpCodeRespondData()
    {
        $response = $this->_createGetRequest(new class ($this->logger) extends Action
        {
            public function __construct(
                LoggerInterface $loggerInterface
            ) {
                parent::__construct($loggerInterface);
            }

            public function action(): Response
            {
                return $this->respondWithData("200 but with data");
            }
        });

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testActionSetsHttpCodeRespondUnauthorized()
    {
        $response = $this->_createGetRequest(new class ($this->logger) extends Action
        {
            public function __construct(
                LoggerInterface $loggerInterface
            ) {
                parent::__construct($loggerInterface);
            }

            public function action(): Response
            {
                return $this->respondUnauthorized();
            }
        });

        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testActionSetsParams()
    {
        $response = $this->_createGetRequest((new class ($this->logger) extends Action
        {
            public function __construct(
                LoggerInterface $loggerInterface
            ) {
                parent::__construct($loggerInterface);
            }

            public function action(): Response
            {
                if (!empty($this->resolveArg('testParam')) && $this->resolveArg('testParam') === 'john') {
                    return $this->respondWithData("Param exists");
                } else {
                    return $this->respondWithCode(404, "Param not found");
                }
            }
        }), '/john');

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testActionSetsParamsMissing()
    {
        $this->expectException(HttpNotFoundException::class);

        $this->_createGetRequest((new class ($this->logger) extends Action
        {
            public function __construct(
                LoggerInterface $loggerInterface
            ) {
                parent::__construct($loggerInterface);
            }

            public function action(): Response
            {
                if (!empty($this->resolveArg('testParam')) && $this->resolveArg('testParam') === 'john') {
                    return $this->respondWithData("Param exists");
                } else {
                    return $this->respondWithCode(404, "Param not found");
                }
            }
        }), '/john/missing');
    }
}
