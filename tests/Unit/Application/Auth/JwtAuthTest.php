<?php

declare(strict_types=1);

namespace Tests\Application\Actions;

use App\Application\Actions\ActionError;
use App\Application\Actions\ActionPayload;
use App\Application\Handlers\HttpErrorHandler;

use DI\Container;
use Slim\Middleware\ErrorMiddleware;
use Tests\TestCase;

use App\Application\Actions\Quote\ViewQuoteAction;
use App\Application\Middleware\JwtMiddleware;
use App\Domain\Exception\QuoteNotFoundException;
use App\Domain\Quote\QuoteRepository;
use App\Infrastructure\Persistance\Disk\Quote\QuotesFromDiskRepository;
use Psr\Log\LoggerInterface;

use Prophecy\PhpUnit\ProphecyTrait;
use App\Application\Auth\JwtAuth;
use InvalidArgumentException;
use Lcobucci\JWT\Builder;

class JwtAuthTest extends TestCase
{
    use ProphecyTrait;

    public function testValidToken()
    {
        $jwtInstance = new JwtAuth(
            getenv('API_AUTH_ISSUER'),
            (int) getenv('API_AUTH_LIFETIME'),
            file_get_contents(ROOT . getenv('API_AUTH_KEY_PRIVATE')),
            file_get_contents(ROOT . getenv('API_AUTH_KEY_PUBLIC'))
        );

        // 1 Year token
        $unlimitedToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjJmYzNmNjY4LTgyYTktNDNiNC05ZGU4LWQ2ZDZkNzEyZWQ3YiJ9.eyJpc3MiOiJBUFBfVVJMIiwianRpIjoiMmZjM2Y2NjgtODJhOS00M2I0LTlkZTgtZDZkNmQ3MTJlZDdiIiwiaWF0IjoxNTk3NDQ0Mzc4LCJuYmYiOjE1OTc0NDQzNzgsImV4cCI6MTYyODQ0NDM3OCwidXNlciI6InRlc3RAaXByZXNlbmNlLmNvbSIsInVpZCI6MH0.eDzkxNEipngj6WieKeXx7vworG_xZQOFq5cUZ2PBsoqwB49-aYdUbvFSLf6aU230JZSk14yFybao2zNdZv3cTB4PZ5XL7zKyO8ktVzZLRBnxvX38WzgtIgBJDetH93v3ksWvBSci2_MOjfRQySTsfMMCPg__6iJJQaW_47V_g_4MCvs8ZoGhYWNpvaLh_SyblLt7VCKPlKvUF4DWr0tcN5PFLDFkcAtPymxrasapPS2GRo6bp0eUn7Wr_fed7kHIIlxJYSksSJWAGSTjxb0ykMFumqPl53jXZoMjpzB9Ux4wXZzNhiuK8RPpJhbayOdfDclcssnDCS1aaT_rwm1rAg";

        $this->assertEquals(true, $jwtInstance->validateToken($unlimitedToken));
    }

    public function testInvalidatFormatToken()
    {
        $jwtInstance = new JwtAuth(
            getenv('API_AUTH_ISSUER'),
            (int) getenv('API_AUTH_LIFETIME'),
            file_get_contents(ROOT . getenv('API_AUTH_KEY_PRIVATE')),
            file_get_contents(ROOT . getenv('API_AUTH_KEY_PUBLIC'))
        );

        // Invalid tokenn
        $unlimitedToken = "g";

        $this->expectException(InvalidArgumentException::class);

        $validated = $jwtInstance->validateToken($unlimitedToken);

        $this->assertEquals(false, $validated);
    }

    public function testOldToken()
    {
        $jwtInstance = new JwtAuth(
            getenv('API_AUTH_ISSUER'),
            (int) getenv('API_AUTH_LIFETIME'),
            file_get_contents(ROOT . getenv('API_AUTH_KEY_PRIVATE')),
            file_get_contents(ROOT . getenv('API_AUTH_KEY_PUBLIC'))
        );

        // Old token
        $unlimitedToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImE2YjhkNDQzLTZkYzctNGM1My05ZTI0LTdkOTUzYjQ2N2M2MyJ9.eyJpc3MiOiJBUFBfVVJMIiwianRpIjoiYTZiOGQ0NDMtNmRjNy00YzUzLTllMjQtN2Q5NTNiNDY3YzYzIiwiaWF0IjoxNTk3NDQ0MDA4LCJuYmYiOjE1OTc0NDQwMDgsImV4cCI6MTU5NzQ0NDAwOCwidXNlciI6InRlc3RAaXByZXNlbmNlLmNvbSIsInVpZCI6MH0.VpI_9AYzPAyvMvPH9ht6uBw6R-IvLTSgiboHfhqz8lueDfacvbbaQ7oToBamJ2oGELFiD6FT3Q6wsJhZfMXSY2zbHLXEyPF59eH6Y8cj3nFUCIyfo5BY-_UntztUTH0TlaYhrCfHWO5-7uyKcKcDiPpVsLPo0swlSonMWu-U0F6WX8GMxpvb9BdeH45FBWkuqioqzZh6JSEffohjxpBmCVn0KSJV9EtvZnKcpxKLkCvsYTGwIyLvB2ojvBnN6NXRrl7sT0SFhPfCtVORv0mtQmJnxuVRX66tLvv_z8s3_CkorxC35-uIPIT2YYYLHebsaMkn552l9bX17p7e7fmA5Q";

        $validated = $jwtInstance->validateToken($unlimitedToken);

        $this->assertEquals(false, $validated);
    }

    public function testCreateToken()
    {
        $jwtInstance = new JwtAuth(
            getenv('API_AUTH_ISSUER'),
            (int) getenv('API_AUTH_LIFETIME'),
            file_get_contents(ROOT . getenv('API_AUTH_KEY_PRIVATE')),
            file_get_contents(ROOT . getenv('API_AUTH_KEY_PUBLIC'))
        );

        $token = $jwtInstance->createJwt("test@ipresence.com", 0);

        $this->assertEquals(true, $jwtInstance->validateToken($token));
    }
}
