<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\Quote;

use Tests\TestCase;
use App\Domain\Quote\Quote;
use App\Domain\Quote\QuoteCollection;

class QuoteCollectonTest extends TestCase
{
    public function testQuoteCollectionConstructor()
    {
        $quoteCollection = new QuoteCollection(
            new Quote('James Franklin', 'Some proficent quote'),
            new Quote('Jimmy Joffa', 'Random quote'),
            new Quote('Sammuel L. Jackson', 'Test quote'),
            new Quote('Keith Richards', 'Another Quote')
        );

        // Counteable from \Countable
        $this->assertEquals(4, count($quoteCollection));
    }

    public function testAddQuoteCollection()
    {
        $quoteCollection = new QuoteCollection();

        $quote1 = new Quote('James Franklin', 'Some proficent quote');
        $quote2 = new Quote('Mike Jagger', 'Test quote');
        $quote3 = new Quote('Napoleon', 'Working hard');

        $quoteCollection->add($quote1);
        $quoteCollection->add($quote2);
        $quoteCollection->add($quote3);

        // Counteable from \Countable
        $this->assertEquals(3, count($quoteCollection));
    }

    public function testGetQuoteCollection()
    {
        $quoteCollection = new QuoteCollection(
            new Quote('James Franklin', 'Some proficent quote'),
            new Quote('Jimmy Joffa', 'Random quote'),
            new Quote('Sammuel L. Jackson', 'Test quote'),
            new Quote('Keith Richards', 'Another Quote')
        );

        $expectJoffa = $quoteCollection->offsetGet(1);

        // Counteable from \Countable
        $this->assertEquals('Jimmy Joffa', $expectJoffa->getAuthor());
    }

    public function testGetQuoteCollectionArrayOfMessages()
    {
        $quoteCollection = new QuoteCollection(
            new Quote('James Franklin', 'Some proficent quote'),
            new Quote('Jimmy Joffa', 'Random quote')
        );

        $expectedArray = [
            'SOME PROFICENT QUOTE!',
            'RANDOM QUOTE!'
        ];

        $quoteArray = $quoteCollection->getQuoteListArrayShout();

        // Counteable from \Countable
        $this->assertEquals(2, count($quoteArray));
        $this->assertEquals($expectedArray, $quoteArray);
    }

    public function testRemoveQuoteFromCollection()
    {
        $quoteCollection = new QuoteCollection(
            new Quote('James Franklin', 'Some proficent quote'),
            new Quote('Sammuel L. Jackson', 'Test quote'),
            new Quote('Jimmy Joffa', 'Random quote')
        );

        // Remove Samuel L. Jackson
        if ($quoteCollection->offsetExists(1)) {
            $quoteCollection->offsetUnset(1);
        }

        $expectedArray = [
            'SOME PROFICENT QUOTE!',
            'RANDOM QUOTE!'
        ];

        $this->assertEquals($expectedArray, $quoteCollection->getQuoteListArrayShout());
    }

    public function testRemoveQuoteWithBadKeyFromCollection()
    {
        $quoteCollection = new QuoteCollection(
            new Quote('James Franklin', 'Some proficent quote'),
            new Quote('Sammuel L. Jackson', 'Test quote'),
            new Quote('Jimmy Joffa', 'Random quote')
        );

        $this->assertEquals(null, $quoteCollection->offsetUnset(100));
    }

    public function testRemoveQuoteFromCollectionByQuoteEntity()
    {
        $jimmyJoffa = new Quote('Jimmy Joffa', 'Random quote');

        $quoteCollection = new QuoteCollection(
            new Quote('Albert Einstein', 'Some proficent quote'),
            new Quote('Sammuel L. Jackson', 'Test quote'),
            $jimmyJoffa,
            new Quote('Mike Tyson', 'Some testing text'),
            new Quote('Luciano Pavarotti', 'Bella dona')
        );


        $quoteCollection->offsetUnset($jimmyJoffa);

        $expectedArray = [
            "SOME PROFICENT QUOTE!",
            "TEST QUOTE!",
            "SOME TESTING TEXT!",
            "BELLA DONA!"
        ];

        $this->assertEquals($expectedArray, $quoteCollection->getQuoteListArrayShout());
    }

    public function testQuoteCollectionFindByQuoteEntity()
    {
        $jimmyJoffa = new Quote('Jimmy Joffa', 'Random quote');

        $quoteCollection = new QuoteCollection(
            new Quote('Albert Einstein', 'Some proficent quote'),
            new Quote('Sammuel L. Jackson', 'Test quote'),
            $jimmyJoffa,
            new Quote('Mike Tyson', 'Some testing text'),
            new Quote('Luciano Pavarotti', 'Bella dona')
        );

        $this->assertEquals(true, $quoteCollection->offsetExists($jimmyJoffa));
    }

    public function testCleanQuoteCollection()
    {
        $quoteCollection = new QuoteCollection(
            new Quote('James Franklin', 'Some proficent quote'),
            new Quote('Sammuel L. Jackson', 'Test quote'),
            new Quote('Jimmy Joffa', 'Random quote')
        );

        $quoteCollection->clear();

        // Counteable from \Countable
        $this->assertEquals(0, count($quoteCollection));
    }

    public function testQuoteCollectionGetIterator()
    {
        $quoteCollection = new QuoteCollection(
            new Quote('James Franklin', 'Some proficent quote'),
            new Quote('Sammuel L. Jackson', 'Test quote'),
            new Quote('Jimmy Joffa', 'Random quote')
        );

        $quoteCollectionIterator = $quoteCollection->getIterator();

        $cur = $quoteCollectionIterator->current();

        // Counteable from \Countable
        $this->assertEquals("James Franklin", $cur->getAuthor());
    }
}
