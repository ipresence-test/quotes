<?php

declare(strict_types=1);

namespace Tests\Unit\Domain\Quote;

use Tests\TestCase;
use App\Domain\Quote\Quote;
use App\Domain\Exception\InvalidArgumentException;

class QuoteTest extends TestCase
{
    public function testQuoteIsObject()
    {
        $quote = new Quote('James Franklin', 'Some proficent quote');

        $this->assertEquals(true, method_exists($quote, 'shout'));
    }

    public function testQuoteShouldReturnNullOnEmpty()
    {
        $this->expectException(InvalidArgumentException::class);

        $quote = new Quote('James Franklin', '');
        $this->assertEquals(null, $quote->shout());
    }

    public function testQuoteShouldReturnQuote()
    {
        $quote = new Quote('Kurt Cobain', 'Some test');

        $this->assertEquals("Some test", $quote->getQuote());
    }

    public function testQuoteEmptyAuthor()
    {
        $this->expectException(InvalidArgumentException::class);

        $quote = new Quote('', 'Quote');

        $this->assertEquals(null, $quote->shout());
    }

    public function testQuoteOneLetter()
    {
        $quote = new Quote('James Hetfield', 't');

        $this->assertEquals("T!", $quote->shout());
    }

    public function testQuoteExclamationOnly()
    {
        $quote = new Quote('Lita Ford', '!');

        $this->assertEquals("!", $quote->shout());
    }

    public function testQuoteEndingWithDotShuldBeUpper()
    {
        $quote = new Quote('Steve Jobs', 'The only way to do great work is to love what you do.');

        $this->assertEquals("THE ONLY WAY TO DO GREAT WORK IS TO LOVE WHAT YOU DO!", $quote->shout());
    }

    public function testQuoteEndingWithExclamationMustBeUpper()
    {
        $quote = new Quote('Samuel Jackson', 'Your time is limited, so don’t waste it living someone else’s life!');

        $this->assertEquals("YOUR TIME IS LIMITED, SO DON’T WASTE IT LIVING SOMEONE ELSE’S LIFE!", $quote->shout());
    }

    public function testQuoteMustEndWithExclamation()
    {
        $quote = new Quote('Milton Berle', 'If opportunity doesn’t knock, build a door');

        $this->assertEquals("IF OPPORTUNITY DOESN’T KNOCK, BUILD A DOOR!", $quote->shout());
    }

    public function testQuoteWithEllipsis()
    {
        $quote = new Quote('Milton Berle', 'If opportunity doesn’t knock, build a door...');

        $this->assertEquals("IF OPPORTUNITY DOESN’T KNOCK, BUILD A DOOR...!", $quote->shout());
    }
}
