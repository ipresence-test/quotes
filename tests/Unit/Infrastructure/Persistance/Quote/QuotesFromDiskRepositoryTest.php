<?php

declare(strict_types=1);

namespace Tests\Application\Actions;

use Tests\TestCase;
use App\Infrastructure\Persistance\Disk\Quote\QuotesFromDiskRepository;
use App\Common\FileHandler;
use App\Common\UrlParameterFormat;
use App\Domain\Exception\QuoteNotFoundException;
use App\Domain\Quote\Quote;

class QuotesFromDiskRepositoryTest extends TestCase
{
    public function testFindByUserName()
    {
        $fileHandler = new FileHandler(ROOT . getenv('QUOTE_LIST'));

        $decodedFile = $fileHandler->decodeJsonFIle();

        $quoteRepoDisk = new QuotesFromDiskRepository($decodedFile);

        $quote = $quoteRepoDisk->findOneByAuthor("Robert Frost");

        $this->assertEquals('Robert Frost', $quote->getAuthor());
    }

    public function testFindFailWithUsername()
    {
        $fileHandler = new FileHandler(ROOT . getenv('QUOTE_LIST'));

        $decodedFile = $fileHandler->decodeJsonFIle();

        $quoteRepoDisk = new QuotesFromDiskRepository($decodedFile);

        $this->expectException(QuoteNotFoundException::class);

        $quote = $quoteRepoDisk->findOneByAuthor("John Travolta");

        $this->assertEquals(null, $quote);
    }

    public function testGetListBadAuthor()
    {
        $fileHandler = new FileHandler(ROOT . getenv('QUOTE_LIST'));

        $decodedFile = $fileHandler->decodeJsonFIle();

        $quoteRepoDisk = new QuotesFromDiskRepository($decodedFile);

        $quote = $quoteRepoDisk->paginateByAuthor("John Travolta");

        $this->assertEquals(null, $quote);
    }

    public function testGetListValidAuthorHasOne()
    {
        $fileHandler = new FileHandler(ROOT . getenv('QUOTE_LIST'));

        $decodedFile = $fileHandler->decodeJsonFIle();

        $quoteRepoDisk = new QuotesFromDiskRepository($decodedFile);

        $quote = $quoteRepoDisk->paginateByAuthor("Woody Allen", 1);

        $this->assertEquals(1, $quote ? count($quote) : null);
    }

    public function testGetListValidAuthorHasTwo()
    {
        $fileHandler = new FileHandler(ROOT . getenv('QUOTE_LIST'));

        $decodedFile = $fileHandler->decodeJsonFIle();

        $quoteRepoDisk = new QuotesFromDiskRepository($decodedFile);

        $quotes = $quoteRepoDisk->paginateByAuthor("Zig Ziglar", 2);

        $this->assertEquals(2, $quotes ? count($quotes) : null);
    }

    public function testGetListValidPaginationButNull()
    {
        $fileHandler = new FileHandler(ROOT . getenv('QUOTE_LIST'));

        $decodedFile = $fileHandler->decodeJsonFIle();

        $quoteRepoDisk = new QuotesFromDiskRepository($decodedFile);

        $quote = $quoteRepoDisk->paginateByAuthor('', 3);

        $this->assertEquals(null, $quote ? count($quote) : null);
    }
}
